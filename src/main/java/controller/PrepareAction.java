package controller;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.apache.lucene.document.Document;
//import org.apache.lucene.index.IndexableField;

import util.Utils;
import vn.hus.nlp.tokenizer.VietTokenizer;
import vn.hus.nlp.utils.TextFileFilter;
import vn.hus.nlp.utils.FileIterator;
import constant.IConstant;
import entities.*;

public class PrepareAction {

	public static void tokenDocument(String inputDir, String outputDir){
		VietTokenizer tokenizer=new VietTokenizer(IConstant.TOKEN_PROP);
		
		tokenizer.tokenizeDirectory(inputDir, outputDir);		
	}
	/*
	public static List<Article> taggerArticle(List<Article> lstArticle)
	{
		List<Article> lstNew = new ArrayList<Article>();
		for(Article article: lstArticle)
		{
			//tagged article content
			
			List<Clause> lstClause = article.getClauses();
			
			for(Clause clause: lstClause)
			{
				//tagged clause content
				
				List<Point> lstPoint = clause.getPoints();
				
				for(Point point: lstPoint)
				{
					//tagged point content
					
					
				}
			}
		}
		return lstNew;
	}*/
	
	/**
	 * Buoc 1: Khoi tao cac doi tuong
	 * + Article
	 * + Clause
	 * + Point
	 */
	public static List<Article> parseDocument(String inputDir)
			throws IOException {
		//String outputDir = IConstant.LAW_TOKEN_DIR;
		TextFileFilter fileFilter = new TextFileFilter(".txt");
		File inputDirFile = new File(inputDir);
		// get the current dir
		//String currentDir = new File(".").getAbsolutePath();

		// get all input files
		File[] inputFiles = FileIterator.listFiles(inputDirFile, fileFilter);
		// System.out.println("Tagging all files in the directory, please wait..."
		// + inputDir);

		List<Article> articles = new ArrayList<Article>();

		//long startTime = System.currentTimeMillis();
		for (File aFile : inputFiles) {
			FileInputStream is = new FileInputStream(aFile);
			String fileName = aFile.getName();
			System.out.print(fileName);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			String text = new String(buffer);

			int flags = Pattern.UNICODE_CHARACTER_CLASS
					| Pattern.CASE_INSENSITIVE;
			Pattern pat = Pattern.compile(Article.regexp, flags);
			Matcher match = pat.matcher(text);
			int count = 0;
			int start = 0;
			int end = 0;

			Matcher mchap = Pattern.compile(Article.regChapter, flags).matcher(
					text);
			List<String> chapt = new ArrayList<String>();
			List<Integer> bchap = new ArrayList<Integer>(), bsec = new ArrayList<Integer>();
			while (mchap.find()) {
				bchap.add(mchap.start());
				chapt.add(mchap.group());
			}

			Matcher msec = Pattern.compile(Article.regSection, flags).matcher(
					text);
			List<String> section = new ArrayList<String>();
			while (msec.find()) {
				bsec.add(msec.start());
				section.add(msec.group());
			}

			//LawType e = null;
			String[] name = fileName.split("___");
			String doc_til = null;
			double boost = 0;
			if (fileName.contains("luat")) {
				//e = LawType.LUAT;
				doc_til = "Luật Doanh Nghiệp 2005";
				boost = 3;
			}
			else if (fileName.contains("NĐ")) {
				//e = LawType.NGHI_DINH;
				//e.setName("NGHI_DINH");
				doc_til = "Nghị Định Số " + name[0] + " Năm " + name[1];
				boost = 1;
			}
			else if (fileName.contains("QÐ")) {
				//e = LawType.QUYET_DINH;
				//e.setName("QUYET_DINH");
				doc_til = "Quyết Định Số " + name[0] + " Năm " + name[1];
				boost = 0.8;
			}
			else if (fileName.contains("QH")) {
				//e = LawType.NGHI_QUYET;
				//e.setName("NGHI_QUYET");
				doc_til = "Nghị Quyết Số " + name[0] + " Năm " + name[1];
				boost = 1;
			}
			else{
				//e = LawType.THONG_TU;
				//e.setName("THONG_TU");
				doc_til = "Thông Tư Số " + name[0] + " Năm " + name[1];
				boost = 0.5;
			}
			int isec = 0; // index cua muc
			int ichap = 0; // index cua chuong
			boolean skip = false;
			while (match.find()) { // loc ra cac dieu luat
				// pos.add(match.start());
				count++;
				skip = false;
				if (count == 1) {
					start = match.start();
					continue;
				}

				if (bsec.size() - 1 > isec && bsec.get(isec + 1) < start)
					isec++;
				if (bchap.size() - 1 > ichap && bchap.get(ichap + 1) < start)
					ichap++;

				end = match.start();
				if (bchap.size() > ichap + 1) {
					if (bchap.get(ichap + 1) < end) {
						end = bchap.get(ichap + 1);
						skip = true;
					}
				}
				if (bsec.size() > isec + 1) {
					if (bsec.get(isec + 1) < end) {
						end = bsec.get(isec + 1);
						skip = true;
					}
				}

				String rule = text.substring(start, end);

				Article art = new Article(rule);
				if (section.size() > 0)
					art.setSec_num(section.get(isec));
				if (chapt.size() > 0)
					art.setChap_num(chapt.get(ichap));

				//art.setLaw_type(e);

				art.setDoc_title(doc_til);
				art.setBoost(boost);
				articles.add(art);
				start = end;
				if (skip)
					start = match.start();

			}
			if (count > 0) {
				Article art = new Article(text.substring(start,
						text.length() - 1));
				art.setDoc_title(doc_til);
				art.setBoost(boost);
				//art.setLaw_type(e);
				articles.add(art);
			}

			System.out.println(" So dieu : " + count);
			is.close();
		}
		return articles;
	}
	
	/**
	 * Buoc 1: Danh chi muc chua du lieu
	 */
	public static void indexData(List<Article> lstArticle) {						
		long startTime = new Date().getTime();
		Index index = new Index(lstArticle);
		index.indexdata(true);
		long endTime = new Date().getTime();

		System.out.println("Index " + IConstant.LAW_TOKEN_DIR + " directory took "
				+ (endTime - startTime) + " milliseconds.");
	}

	/**
	 * Buoc 2: Xu ly cau Query bao gom: + tach cum tu + tach tu khoa + loai bo
	 * stopwords
	 */	

	/**
	 * 1.Loai bo cac tu, ky hieu khong can thiet trong cau truy van
	 * 2.Tach tu + Gan nhan cau Truy van
	 * su dung thu vien vntagger - Le Hong Phuong
	 * @param tokenize
	 * @return
	 * @throws IOException 
	 */
	public static String taggerExecute(String tokenize) throws IOException {
		tokenize = tokenize.trim().toLowerCase();
		//loai bo tu hoi
		for(String unsual: IConstant.questionWords)
		{
			tokenize = tokenize.replace(unsual, "");
		}
		//loai bo ki tu dac biet
		for(String specChar: IConstant.specialCharacters)
		{
			tokenize = tokenize.replace(specChar, "");
		}
		
		tokenize = getAcronyms(tokenize);
		
		
		VietTokenizer token=new VietTokenizer(IConstant.TOKEN_PROP);
		System.out.print(IConstant.TOKEN_PROP);
		tokenize = token.segment(tokenize);
		//String[] tokenEdit = tokenize.split("\\s+");
		List<String> stopWords = Utils.readTermsFile(IConstant.STOPWORD);
		tokenize = " " + tokenize + " ";
		for(String stop: stopWords)
		{
			tokenize = tokenize.replace(" " + stop + " ", " ");
		}
		
		//tagged query
		//VietnameseMaxentTagger tagger = new VietnameseMaxentTagger(IConstant.MODEL_DIR + "\\vtb.tagger");		
		//TaggerOptions.UNDERSCORE=true;
		return tokenize.trim();
		//return tagger.tagText(tokenize);
	}
	
	/**
	 * xu ly cac tu viet tat trong cay Query
	 * @param Query
	 * @return cau Query da duoc xu ly
	 */
	public static String getAcronyms(String Query)
	{
		System.out.println("Replacing Acronym...");
		for(int i = 0; i < IConstant.nowWords.length; i++)
		{
			String nowWord = IConstant.nowWords[i];
			if(Query.contains(nowWord))
			{
				Query = Query.replaceAll(nowWord,IConstant.orgWords[i][0]);
				for(int j = 1; j < IConstant.orgWords[i].length; j++)
				{
					Query += " " + IConstant.orgWords[i][j];
				}
			}
		}
		return Query;		    
	}
	
	/**
	 * them cac tu dong nghia trong cau Query
	 * @param Query
	 * @return tra ve cau Query chua cac tu dong nghia
	 * @throws IOException 
	 */
	public static String getSynonyms(String Query) throws IOException
	{
		System.out.println("Adding Synonyms...");
		List<String> synonyms = Utils.readTermsFile(IConstant.Synonyms_token);
		/*
		String newQuery = " ";
		String[] q = Query.split(" ");
		for(String qu: q)
		{
			qu = "+" + qu + " ";
			newQuery = newQuery + qu;
		}
		Query = newQuery;
		*/
		Query = " " + Query + " ";
		for(String syn: synonyms)
		{
			String[] Word = syn.split(":");
			String[] nowWord = Word[1].split(",");
			String orgWord = Word[0];
			for(String w: nowWord)
				if(Query.contains(" " + w + " "))
				{
					String wordReplacement = "(" + orgWord + " OR " + w +")";
					//String wordReplacement = orgWord;
					Query = Query.replaceAll(w, wordReplacement);
					break;
				}							
		}
		return Query.trim();
	}
	
	/**
	 * xay dung cau query (quan trong)
	 * 
	 * @param query
	 *            raw query dua vao
	 * @return list cau query da duoc xu ly
	 * @throws IOException
	 */
	public static String QueryBuilder(String query) throws IOException {
		System.out.println("Start Building Query...");		
		// tach tu & gan nhan, dong thoi loai bo stopwords
		System.out.println("Start Tagger Query...");
		String Query = taggerExecute(query);
		Query = getSynonyms(Query);
		System.out.println("Result: " + Query);		
		/*
		List<String> lstTwoTerms = Utils.readTermsFile(IConstant.vnTwoTerms);
		List<String> lstThreeTerms = Utils.readTermsFile(IConstant.vnTwoTerms);

		// Lấy danh sách stopWords
		List<String> stopWords = Utils.readTermsFile(IConstant.vnStopWordsFile);
		
		// Danh từ - Danh từ riêng - Danh từ chỉ loại - Danh từ viết tắt - Danh
		// từ đơn vị - Số từ
		Query = Query.replaceAll("/Np|/Nc|/Nu", "/N");
		String[] aTokenize = Query.split(" ");
		String mainQuery = "", subQuery = "";

		// Xét cụm danh từ
		System.out.println("Start Get Noun Phrases...");
		List<String> aNounPhrase = getNounPhrase(aTokenize, 3, stopWords, false);
		do {
			for (String str : aNounPhrase) {
				String[] re = getQueryByWordPhrase(aTokenize, lstTwoTerms,
						lstThreeTerms, mainQuery, subQuery, str);
				mainQuery = re[0];				
				subQuery = re[1];
			}
			aNounPhrase = getNounPhrase(aTokenize, 3, stopWords, false);
		} while (aNounPhrase.size() > 0);

		aNounPhrase = getNounPhrase(aTokenize, 2, stopWords, false);
		for (String str : aNounPhrase) {
			String[] re = getQueryByWordPhrase(aTokenize, lstTwoTerms,
					lstThreeTerms, mainQuery, subQuery, str);
			mainQuery = re[0];
			subQuery = re[1];
		}

		// Xét cụm động từ
		System.out.println("Start Get Verb Phrases...");
		List<String> aVerbPhrase = getVerbPhrase(aTokenize, 3, stopWords, false);
		for (String str : aVerbPhrase) {
			String[] re = getQueryByWordPhrase(aTokenize, lstTwoTerms,
					lstThreeTerms, mainQuery, subQuery, str);
			mainQuery = re[0];
			subQuery = re[1];
		}

		aVerbPhrase = getVerbPhrase(aTokenize, 2, stopWords, false);
		for (String str : aVerbPhrase) {
			String[] re = getQueryByWordPhrase(aTokenize, lstTwoTerms,
					lstThreeTerms, mainQuery, subQuery, str);
			mainQuery = re[0];
			subQuery = re[1];
		}

		// Xét các từ khác
		List<String> anOtherWords = getOtherWords(aTokenize, stopWords);
		for (String word : anOtherWords) {
			if (!mainQuery.equals("")) {
				mainQuery += " AND " + "\"" + word + "\"~";
			}
		}		
		System.out.print("mainQ: " + mainQuery);
		//System.out.print("subQ: " + subQuery);
		*/
		return Query;
		
	}

	/**
	 * Buoc 3: dua cau query vao bo may tim kiem lucene
	 *  + Lay noi dung cua duong dan tim kiem tra ve
	 */
	
	/**
	 * tim kiem du lieu dung thu vien search cua Lucene 
	 * 
	 * @param queryStr
	 *            : cau truy van da duoc xu ly phu 
	 *            hop voi Lucene
	 * @return Danh sach cac tap phu hop
	 * @throws Exception
	 */
	public static List<Document> searchData(String queryStr, boolean fromGoogle)
			throws Exception {					
			try {
				//String result = new String();
				Search search = new Search(IConstant.INDEX_DIR, queryStr);						
				//List<Document> lstDoc = new ArrayList<Document>();
				return search.search();
				//int i = 0;
				/*
				if (lstDoc.size() == 0) {
					result = "Không tìm thấy câu trả lời phù hợp";
				}
				else{
					
					for(Document doc:lstDoc)
					{		
						System.out.print("name: " + doc.get("name"));
						System.out.print("score: " + doc.get("score"));
						String name = doc.get("name").replaceAll("_", " ");
						if (name != null) {			
							result += "<br>" + (i + 1) + ". " + name + "\t score="
									+ doc.get("score") + "</br>" + "<p>"
									+ doc.get("article").replaceAll("_", " ").replace(name, "") + "</p><br/>";

							String title = doc.get("title").replaceAll("_", " ");
							if (title != null) {
								System.out.println("   Title: " + doc.get("title"));
							}
						} else {
							System.out
									.println((i + 1) + ". " + "No path for this document");
						}
						i++;
					}
					
				}
				*/
				//return result;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		

		return null;
	}	

	/**
	 * Buoc 5: Dung LSI tinh do tuong dong giua cau query va cac tap ung vien
	 * Chon ra 3 cau tra loi co do tuong dong cao nhat
	 */
	
	public static String prepareString(String article) throws IOException
	{
		
		for (String s: IConstant.specialCharacters)
			article = article.replace(s, "");		
		return article;
	}
	
	/**
	 * Tach cac tu phan biet trong danh sach article, query
	 * phuc vu cho viec xay dung ma tran LSI
	 * @param lstPara
	 * @return
	 * @throws IOException
	 */
	public static List<String> getTermQuery(String Query) throws IOException
	{
		List<String> lstTerm = new ArrayList<String>();
		String[] aWord = Query.split(" ");
		List<String> stopWords = Utils.readTermsFile(IConstant.STOPWORD);
		for(String w : aWord)
		{
			if (!w.equals("") && !Utils.checkWordInArray(IConstant.exceptTerms, w) && 
					!Utils.checkIsDigit(w) && !Utils.checkIsAlpha(w) &&
					!Utils.checkWordInList(stopWords, w))
				if(!lstTerm.contains(w))
					lstTerm.add(w);
		}	
		return lstTerm;
	}
	
	public static List<String> getTerm(List<String> lstDoc, List<String> lstTermQuery) throws IOException 
	{
		List<String> lstTerm = new ArrayList<String>();
		for(String Term: lstTermQuery)
			lstTerm.add(Term);
		String[] aWord;
		List<String> stopWords = Utils.readTermsFile(IConstant.STOPWORD);
		for(String ar: lstDoc)
		{
			if(!ar.equals(""))
			{
				String article = ar.replace("\n", " ").replace("\r", " ");
				article = prepareString(article);
				aWord = article.split(" ");
				
				for(String w : aWord)
				{				
					if (!w.equals("") && !Utils.checkWordInArray(IConstant.exceptTerms, w) && 
							!Utils.checkIsDigit(w) && !Utils.checkIsAlpha(w) &&
							!Utils.checkWordInList(stopWords, w))	
						if(!lstTerm.contains(w))
							lstTerm.add(w);
				}				
				
			}
		}
		//rut gon cac tu dong nghia
		String[] lstNewTerm = new String[lstTerm.size()];
		int c = 0;
		for(String word: lstTerm)
		{
			lstNewTerm[c] = word;
			c++;
		}
		List<String> synonyms = Utils.readTermsFile(IConstant.Synonyms_token);
		for(String syn: synonyms)
		{
			String[] Word = syn.split(":");
			String[] nowWord = Word[1].split(",");
			String orgWord = Word[0];			
			for(String w: nowWord)
			{			
				for(int i = 0; i<lstNewTerm.length; i++)
				{
					if(lstNewTerm[i].equals(w))
					{
						lstNewTerm[i] = orgWord;			
					}
				}
			}
		}
		lstTerm = new ArrayList<String>();
		for(String word: lstNewTerm)
		{
			if(!lstTerm.contains(word))
				lstTerm.add(word);
		}
		return lstTerm;
	}
	
	
	/**
	 * build & tinh ma tran term doc voi cac term o tap ung vien va cau query
	 * @param lstTf
	 * 		list term frequency
	 * @param colsize
	 * @param rowsize
	 * @param lstTerm
	 * 		list term trong tap ung vien
	 * @param Query
	 * 		cau query
	 * @return
	 * @throws IOException 
	 */
	public static double[] calculateMatrix(List<Double> lstTf, int colsize, int rowsize, List<String> lstTerm, String Query) throws IOException
	{
		double[][] termDocAr = new double[rowsize][colsize];
		double[] dfAr = new double[rowsize];
		
		//xay dung term Doc
				
		int c = 0;
		for(int n = 0; n < colsize-1; n++)
			for(int m = 0; m < rowsize; m++)
			{
				double tf = lstTf.get(c);
				
				if(tf != 0)
				{
					/*
					String[] aWord = Query.split(" ");
					for(String word : aWord)
						if(word.equals(lstTermAr.get(m)))
						{
							idfAr[m] += 1;
							//numAr[n] += tf;
							break;
						}
					*/
					dfAr[m] += 1;
				}
				//double lf = Math.log(tf + 1);
				//double pij = tf/(gfAr[m] + 1);
				//double gi = 1 + (pij * Math.log(pij + 1) / Math.log(colsizeAr + 1));
				termDocAr[m][n] = tf;
				c++;
			}
		for(int m = 0; m < rowsize; m++)
		{
			int n = colsize-1;
			String term = lstTerm.get(m);
			double tf = Utils.CountWord(Query, term);
			
			if(tf != 0)
			{
				/*
				if(Query.contains(term))
				{
					idfAr[m] += 1;
					//numAr[n] += tf;
				}
				*/
				dfAr[m] += 1;
			}
			
			//double lf = Math.log(tf + 1);
			//gfAr[m] += tf;
			//double pij = tf/(gfAr[m] + 1);
			//double gi = 1 + (pij * Math.log(pij + 1) / Math.log(n + 1));
			
			termDocAr[m][n] = tf;
			
		}	
		//moi cell la mot trong so tf x idf
		for(int n = 0; n < colsize; n++)
			for(int m = 0; m < rowsize; m++)
			{
				/*
				double lf = 1 + Math.log(termDocAr[m][n] + 1);
				double gf;
				if(dfAr[m] != 0)
					gf = Math.log((colsize+1)/(dfAr[m]));
				else
					gf = 0;
				termDocAr[m][n] = lf*gf;*/
				double tf = termDocAr[m][n];
				double idf;
				if(dfAr[m] == 0)
					idf = 0;
				else
					idf = colsize/dfAr[m];
				if(idf == 0)
					termDocAr[m][n] = 0;
				else
					termDocAr[m][n] = tf * (1+Math.log(idf));
			}
		return Utils.Cosine(termDocAr);
	}
	
	public static double calculatePosMatrix(double[][] PosMat)
	{
		//int n = PosMat[0].length-1;
		int m = PosMat.length;
		double result;
		double sumSubSqrt = 0;
		double sumPlusSqrt = 0;
		for(int j = 0; j < m; j++)
		{
			sumSubSqrt += (PosMat[j][0] - PosMat[j][1])*(PosMat[j][0] - PosMat[j][1]);
			sumPlusSqrt += (PosMat[j][0] + PosMat[j][1])*(PosMat[j][0] + PosMat[j][1]);
		}
		sumSubSqrt = Math.sqrt(sumSubSqrt);
		sumPlusSqrt = Math.sqrt(sumPlusSqrt);
		result = 1-(sumSubSqrt/sumPlusSqrt);
		return result;
	}
	
	public static double createPosMatrix(String Article, String Query) throws IOException
	{
		List<String> lstContentAr = new ArrayList<String>();
		List<Double> lstTfofArticle = new ArrayList<Double>();
		lstContentAr.add(Article);
		List<String> lstTermQuery = new ArrayList<String>();		
		List<String> lstTerm = new ArrayList<String>();
		lstTermQuery = getTermQuery(Query.replace("\n", " ").replace("\r", " "));		
		lstTerm = getTerm(lstContentAr, lstTermQuery);
		int n = lstTerm.size();
		double[] lstPos = new double[n];
		for(int i = 0 ; i < lstPos.length; i++)
		{
			lstPos[i] = i+1;
		}
		double[][] PosMat = new double[n][2];
		for (int i = 0; i < n; i++)
		{
			String word = lstTerm.get(i);
			double Pos = lstPos[i];
			Article = " " + Article + " ";
			if(Article.contains(" " + word + " "))
			{
				PosMat[i][0] = Pos;
			}
			Article = Article.trim();
			Query = " " + Query + " ";
			if(Query.contains(" " + word + " "))
			{
				PosMat[i][1] = Pos;
			}
			Query = Query.trim();
			double tf = Utils.CountWord(Article, word);		
			//lstTfofArticle.add(tf/lstTerm.size());
			if(tf == 0)
				lstTfofArticle.add(tf);
			else
				lstTfofArticle.add(1+Math.log(tf));
		}
		double[] result = calculateMatrix(lstTfofArticle,2,n,lstTerm,Query);
		return result[0] + calculatePosMatrix(PosMat);
	}
	
	/**
	 * Xây dựng mảng Term Document Matrix với mỗi phần tử là giá trị tf
	 * @param isPhrase: mục từ là các cụm từ?, nếu là true thì tất cả các mục từ trong lstTerm đều là cụm có 2 từ trở lên
	 * còn nếu là false thì các mục từ trong lstTerm có thể là cụm một từ hoặc cụm 2 từ lên
	 * @return mãng Term Document Matrix
	 * @throws IOException 
	 */
	public static String termDocumentMatrix(List<Document> lstArticle, String Query) throws IOException{
		
		/*-----------------------------------------------------------
			Tinh aij cua term doc matrix bang cong thuc Log Entropy
			local weighting: lij = log(tfij + 1)
			global weighting: gi = 1 + tong(pij * log(pij) / log(n)) 
			trong do pij = tfij / gfi 
			tf: so lan xuat hien term trong doc
			gf: tong so lan xuat hien term trong doc
			aij = lij * gi
		------------------------------------------------------------*/
		
		//tinh tong so lan xuat hien gf
		//double[] gfAr = new double[lstTermAr.size()];
		//double[] gfCl = new double[lstTermCl.size()];
		//double[] gfPo = new double[lstTermPo.size()];
		String result = "";
		
		if(lstArticle.size() == 0)
			return "<b>Không tìm thấy kết quả !</b> <br/> "
			+ "hãy kiểm tra lại câu hỏi :<br/>"
			+ "1. câu hỏi của bạn có thể không thuộc luật doanh nghiệp Việt Nam. <br/>"
			+ "2. đảm bảo câu hỏi không quá dài, hãy nêu các từ chính bạn muốn hỏi. <br/>"
			+ "3. hệ thống của chúng tôi chưa tốt, mong bạn đóng góp ý kiến.";
		else
		{
			List<Article> lstAr = new ArrayList<Article>();
			for(int i = 0; i < lstArticle.size(); i++)
			{
				lstAr.add(new Article(lstArticle.get(i).get("article")));
				lstAr.get(i).setDoc_title(lstArticle.get(i).get("title"));
				lstAr.get(i).setArt_num(lstArticle.get(i).get("id"));
				lstAr.get(i).setName(lstArticle.get(i).get("name"));
				lstAr.get(i).setChap_num(lstArticle.get(i).get("chapter"));
				lstAr.get(i).setSec_num(lstArticle.get(i).get("section"));
				lstAr.get(i).setScore(lstArticle.get(i).get("score"));
			}
			String newLine = System.getProperty("line.separator");
			//String result = new String();
			/*List<String> lstTermQuery = new ArrayList<String>();
			
			List<String> lstTermAr = new ArrayList<String>();
			List<String> lstContentAr = new ArrayList<String>();
			for(Article ar: lstAr)
			{
				lstContentAr.add(ar.getContent());
			}
			lstTermQuery = getTermQuery(Query.replace("\n", " ").replace("\r", " "));		
			lstTermAr = getTerm(lstContentAr, lstTermQuery);
			*/
			
			
			//mang luu vet
			List<Article> traceAr = new ArrayList<Article>();

			//dem so article, clause, point trong List Article
			//lam column cho term doc matrix
			
			
			
			//danh sach luu trong so tf
			//List<Double> lstTfofArticle = new ArrayList<Double>();
			double[] resultPosAr = new double[lstAr.size()];
			for(int j = 0; j < lstAr.size(); j++)
			{
				Article ar = lstAr.get(j);
				if(!ar.getContent().equals(""))
				{
					traceAr.add(ar);
					
					List<String> synonyms = Utils.readTermsFile(IConstant.Synonyms_token);
					String newContent = ar.getContent();
					for(String syn: synonyms)
					{
						String[] Word = syn.split(":");
						String[] nowWord = Word[1].split(",");
						String orgWord = Word[0];
						for(String word: nowWord)
						{
							newContent = " " + newContent + " ";
							newContent = newContent.replace(" " + word + " "," " + orgWord+ " ");
							newContent = newContent.trim();
							if(j == 0 )
							{
								Query = " " + Query +" ";
								Query = Query.replace(" " + word + " "," " + orgWord+ " ");
								Query = Query.trim();
							}
						}
					}				
					resultPosAr[j] = createPosMatrix(newContent,Query);
				}
			}
			
			
			//double[] resultSumary = new double[resultPosAr.length];
			for(int i = 0; i < traceAr.size(); i++)
			{
				double score = resultPosAr[i] +  Double.parseDouble(traceAr.get(i).getScore())/10;
				traceAr.get(i).setScore(String.valueOf(score));
			}
			
			/*
			for(int i = 0; i<traceAr.size()-1;i++)
			{
				for(int j = i+1; j<traceAr.size();j++)
				{
					double numA = Double.parseDouble(traceAr.get(i).getScore());
					double numB = Double.parseDouble(traceAr.get(j).getScore());
					if(numA < numB)
					{
						Article temp1 = traceAr.get(i);
						traceAr.set(i, traceAr.get(j));
						traceAr.set(j, temp1);
					}
				}			
			}*/
			//List<Article> sortTraceAr = new ArrayList<Article>();
			//List<Clause> sortTraceCl = new ArrayList<Clause>();
			//List<Point> sortTracePo = new ArrayList<Point>();
			List<String> lstResult = new ArrayList<String>();
			List<Double> lstScore = new ArrayList<Double>();
			System.out.print("-------------------------- ");
			System.out.print(newLine);
			System.out.print("TF*IDF:");
			System.out.print(newLine);
			System.out.print("-------------------------- ");
			System.out.print(newLine);
			for (int i = 0; i < traceAr.size(); i++)
			{	
				double num = Double.parseDouble(traceAr.get(i).getScore());				
				boolean isDelAr = false;
				
				//if(num > IConstant.Articlethreshold || i < 4)
					//sortTraceAr.add(traceAr.get(i));
				
					//System.out.println("dieu khong thoa threshold");			
				List<Clause> traceCl = traceAr.get(i).getClauses();
				if(traceCl.size() != 0)
				{
					/*
					List<String> lstTermCl = new ArrayList<String>();
					List<Double> lstTfofClause = new ArrayList<Double>();
					List<String> lstContentCl = new ArrayList<String>();
					for(Clause cl : traceCl)
					{
						lstContentCl.add(cl.getContent());
					}*/
					//lstTermCl = getTerm(lstContentCl, lstTermQuery);
					//int colsizeCl = 0;
					double[] resultPosCl = new double[traceCl.size()];
					for(int j = 0 ; j < traceCl.size(); j++)
					{
						Clause cl = traceCl.get(j);
						if(!cl.getContent().equals(""))
						{
							//colsizeCl++;
							//traceCl.add(cl);
							List<String> synonyms = Utils.readTermsFile(IConstant.Synonyms_token);
							String newContent = cl.getContent();
							for(String syn: synonyms)
							{
								String[] Word = syn.split(":");
								String[] nowWord = Word[1].split(",");
								String orgWord = Word[0];
								for(String word: nowWord)
								{
									newContent = " " + newContent + " ";
									newContent = newContent.replace(" " + word + " "," " + orgWord+ " ");
									newContent = newContent.trim();				
								}
							}								
							resultPosCl[j] = createPosMatrix(newContent,Query);
						}
					}				
					
					//double[] resultCl = calculateMatrix(lstTfofClause,traceCl.size()+1,lstTermCl.size(),lstTermCl,Query);
					//double[] resultSumaryCl = new double[traceCl.size()];					
					for (int j = 0; j < traceCl.size(); j++)
					{				
						if(resultPosCl[j] > num)
						{
							boolean isDelCl = false;
							isDelAr = true;
							List<Point> tracePo = traceCl.get(j).getPoints();
							if(tracePo.size() != 0)
							{
								
								double[] resultPosPo = new double[tracePo.size()];
								for(int w = 0; w < tracePo.size(); w++)
								{
									Point po = tracePo.get(w);
									if(!po.getContent().equals(""))
									{
										
										List<String> synonyms = Utils.readTermsFile(IConstant.Synonyms_token);
										String newContent = po.getContent();
										for(String syn: synonyms)
										{
											String[] Word = syn.split(":");
											String[] nowWord = Word[1].split(",");
											String orgWord = Word[0];
											for(String word: nowWord)
											{
												newContent = " " + newContent + " ";
												newContent = newContent.replace(" " + word + " "," " + orgWord+ " ");
												newContent = newContent.trim();				
											}
										}					
										resultPosPo[w] = createPosMatrix(newContent,Query);
									}
								}									
								for (int p = 0; p < tracePo.size(); p++)
								{
									if(resultPosPo[p] > resultPosCl[j])
									{
										tracePo.get(p).setScore(resultPosPo[p]);
										tracePo.get(p).setArticleName(traceAr.get(i).getName());
										tracePo.get(p).setTitle(traceAr.get(i).getDoc_title());
										String contentPo = " Căn cứ theo "+tracePo.get(p).getTitle()+": " +"<br/>";
										contentPo += tracePo.get(p).getArticleName().replace("_", " ") +"<br/>";
										contentPo += tracePo.get(p).getContent().replace("_", " ").replace("\n", "<br/>") +"</b></p><br/>";
										lstScore.add(tracePo.get(p).getScore());
										lstResult.add(contentPo);
										isDelCl = true;
										
									}												
								}
							}
							if(isDelCl)
							{
								traceCl.remove(j);
							}
							else
							{							
								traceCl.get(j).setScore(resultPosCl[j]);
								traceCl.get(j).setArticleName(traceAr.get(i).getName());
								traceCl.get(j).setTitle(traceAr.get(i).getDoc_title());
								String contentCl = " Căn cứ theo "+traceCl.get(j).getTitle()+": " +"<br/>";
								contentCl += traceCl.get(j).getArticleName().replace("_", " ") +"<br/>";
								contentCl += traceCl.get(j).getContent().replace("_", " ").replace("\n", "<br/>") +"</b></p><br/>";
								lstScore.add(traceCl.get(j).getScore());
								lstResult.add(contentCl);												
							}
						}									
					}											
				}
				if(isDelAr)
				{
					//traceAr.remove(i);
				}
				else
				{
					String str = " Căn cứ theo "+ traceAr.get(i).getDoc_title() + ": <br/>";
					str += traceAr.get(i).getContent().replace("_", " ").replace("\n", "<br/>")+"</b></p><br/>";
					lstResult.add(str);
					lstScore.add(Double.parseDouble(traceAr.get(i).getScore()));
				}
			}
			for(int i = 0; i<lstScore.size()-1;i++)
			{
				for(int j = i+1; j<lstScore.size();j++)
				{
					if(lstScore.get(i) < lstScore.get(j))
					{
						double t = lstScore.get(i);
						lstScore.set(i, lstScore.get(j));
						lstScore.set(j, t);
						String str = lstResult.get(i);
						lstResult.set(i, lstResult.get(j));
						lstResult.set(j, str);
					}
				}
			}
			for(int i = 0; i < lstResult.size(); i++)
			{
				int pos = i + 1;
				result += "<p><b>"+ "<font color=" + "blue" + "><b>Đáp Án " + pos + ":</b></font> " + lstResult.get(i);
			}
		}
		return result;
		
	}
}