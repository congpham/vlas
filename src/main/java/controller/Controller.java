package controller;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;










import javax.swing.JOptionPane;

import org.apache.lucene.document.Document;
//import org.htmlparser.util.ParserException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;









import constant.IConstant;
//import controller.PrepareAction;
//import entities.JSONException;
import entities.Article;
import entities.Clause;




@org.springframework.stereotype.Controller
public class Controller {
	
	@RequestMapping(value="/ans", method=RequestMethod.POST)	
	public String getQuery(Model m, HttpServletRequest request) throws Exception {	
		request.setCharacterEncoding("UTF-8");	
		String query = "";
		try {
			//PrepareAction.tokenDocument(IConstant.Synonyms, IConstant.Synonyms_token);
			query = request.getParameter("question");
			if(!query.isEmpty() || query.equals(""))
			{
				m.addAttribute("question", query);
				String Query = new String();
				Query = PrepareAction.QueryBuilder(query);
				String result = new String();
				//List<Article> lstArticle = new ArrayList<Article>();
				List<Document> lstDoc = new ArrayList<Document>();
				//Parse Article to Component
				/*lstArticle = PrepareAction.parseDocument(IConstant.LAW_TOKEN_DIR);
				int n = lstArticle.size();
				int cl = 0;
				int po = 0;
				for(Article ar: lstArticle)
				{
					cl += ar.getClauses().size();
					for(Clause c: ar.getClauses())
					{
						po += c.getPoints().size();
					}
				}*/
				//Index
				//PrepareAction.indexData(lstArticle);
				String mainQuery = Query;
				System.out.println("Query: " + mainQuery);
				//Search
				lstDoc = PrepareAction.searchData(mainQuery, false);
				
				//Build Term Doc
				query = PrepareAction.taggerExecute(query);
				result = PrepareAction.termDocumentMatrix(lstDoc, query);
				
				m.addAttribute("result", result);
			}
			else
			{
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "answer";
	}							
	
}