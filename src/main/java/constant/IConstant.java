package constant;

import java.io.File;

public interface IConstant {
	
	//Duong dan can thiet
	public static String HOME_DIR=System.getProperty("user.home")+File.separator+"resources";
	public static String LAW_TOKEN_DIR="vlas"+File.separator+"resources"+File.separator+"law_text_tokenized"+File.separator+"law";
	public static String UNDER_TOKEN_DIR="vlas"+File.separator+"resources"+File.separator+"law_text_tokenized"+File.separator+"under";
	public static String LAW_DATA_DIR=HOME_DIR+File.separator+"law_text"+File.separator+"law";
	public static String UNDER_LAW_DATA_DIR=HOME_DIR+File.separator+"law_text"+File.separator+"under";
	public static String INDEX_DIR=HOME_DIR+File.separator+"index"+File.separator;
	public static String MODEL_DIR = HOME_DIR+File.separator+"models"+File.separator;
	
	
	//public static String synonyms = HOME_DIR + "\\wordnet\\synonyms.xml";
	//public static String acronyms = HOME_DIR + "\\wordnet\\acronyms.xml";
	//Duong dan StopWords
	public static String STOPWORD=HOME_DIR+File.separator+"data"+File.separator+"vnstopword.txt";
	
	public static String Synonyms = HOME_DIR+File.separator+"wordnet"+File.separator+"synonyms.txt";
	public static String Synonyms_token = HOME_DIR+File.separator+"wordnet"+File.separator+"synonyms_token.txt";
	//Token properties cho VietTokenizer
	public static String TOKEN_PROP=HOME_DIR+File.separator+"models"+File.separator+"tokenizer.properties";
	
	public static String[] specialCharacters = {":-", ";-", ")", "(", ":", ".", "-", ";", ".-", ",", "=", "..." , "!", "@", "#", "$", "%"
		, "^" , "&", "*", "?"};
	
	//so luong tap ung vien rut trich
	public static int answerExtractionDocNumber = 10;
	//so luong cau tra loi
	public static int answerSelectionDocNumber = 3;
	//So Chieu Khong Gian LSI
	public static int numFactor = 2;
	
	//nguong xac dinh tap ung vien
	public static double Articlethreshold = 0.2;
	public static double Clausethreshold = 0.002;
	public static double Pointthreshold = 0.1;
	public static double secondaryThreshold = 0.3;
	public static double alpha = 0.5;

	//danh sach tu loai so
	public static String[] numWords = {"một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín", "mười",
									   "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
		
	//danh sach tu hoi ko can thiet
	public static String[] questionWords = {"vậy pháp luật quy định", "được pháp luật quy định như thế nào", "pháp luật quy định như thế nào", "được quy định như thế nào", "pháp luật quy định về", "cần điều kiện gì", "điều kiện gì", 
											"thì phải", "đáp ứng yêu cầu", "đáp ứng những yêu cầu", "theo quy định hiện hành", "theo pháp luật hiện hành", "có được phép", "được thể hiện như thế nào", "được thực hiện như thế nào" ,"thể hiện như thế nào", "những vấn đề gì",
											"pháp luật quy định", "cho tôi hỏi", "tôi muốn hỏi ", "tôi muốn", "xin cho hỏi ", "xin cho biết ", "cho hỏi ", "cho biết ", "phải làm gì", "cần làm gì", "là gì", 
											"thế nào là ", "như thế nào", "bao gồm những gì", "những ai", "những gì", "hay không", "ra sao", "khi nào", "tại sao", "vì sao",
											"trong thời gian bao lâu", "trong bao lâu", "bao lâu", "thế nào", "như thế nào?" , "gì", "nào", "đòi hỏi ", "hỏi ", "việc ", "không ạ", "bị", "của", "theo", " bao gồm", "được xem là"};
		
	//danh sach tu ko can thiet
	public static String[] exceptTerms = {"chương", "điều", "mục","ii", "iii", "iv", 
		"vii", "viii", "ix", "xii", "xiii", "xiv", "xv", "của", "hoặc", "và", "đó", "để", "theo",
		"sự", "việc", "đang", "còn", "nêu", "nếu", "thì", "bị", "mà", "điều kiện cần", "tôi là", "muốn", "bị", 
		"cái", "là", "việc", "theo", "chúng tôi", "hiện nay", "những", "cần", "làm", "hay", "thực hiện", "bao gồm", 
		"có thể", "thì có", "thì", "có", "mức",};//Khi,, "vừa"

	public static String[][] orgWords = {
											{"đăng ký kinh doanh","đăng ký doanh nghiệp","điều kiện kinh doanh"},
											{"chứng nhận đăng ký kinh doanh","chứng nhận đăng ký doanh nghiệp"},
											{"hội đồng thành viên"},	
											{"một thành viên"},
											{"trách nhiệm hữu hạn"},
											{"tư nhân"},
											{"công ty"},											
											{"giấy đề nghị đăng ký doanh nghiệp"},
											{"giấy đăng ký doanh nghiệp"},
											{"giải thể doanh nghiệp"},
											{"giấy chứng nhận"},
											{"hội đồng quản trị"},
											{"chứng minh thư","chứng minh nhân dân"},
											{""},
											{""}
										}; 
		
	public static String[] nowWords = {
											"đkkd",
											"cnđkkd",
											"hđtv",
											"mtv",
											"tnhh",
											"tn",
											"cty",									
											"giấy đề nghị đăng ký kinh doanh",
											"giấy đăng ký kinh doanh",
											"giải thể công ty",
											"gcn",
											"hđqt",
											"cmnd",
											"có",
											"hiện nay"
										}; 
		
}