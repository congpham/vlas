package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.WordlistLoader;
import org.apache.lucene.util.IOUtils;
import org.apache.lucene.util.Version;

import constant.IConstant;

public class Utils {

	/**
	 * ham doc file vao chuoi string
	 * @param pathfile
	 *           duong dan den file can doc
	 * @return chuoi string
	 */
	public static List<String> readTermsFile(String pathFile) throws IOException {
  		List<String> Words = new ArrayList<String>();

  		FileInputStream fi = new FileInputStream(pathFile);
  		InputStreamReader isr = new InputStreamReader(fi, "utf-8");
  		BufferedReader br = new BufferedReader(isr);
  		String tmp = "";
  		while ((tmp = br.readLine()) != null){
  			Words.add(tmp.replaceAll("\r\n", ""));
  		}
  		br.close();
  		return Words;
  	}
	
	/**
	 * Ham doc danh sach cac stopwords tra ve dinh dang charArraySet
	 * @param stopwords
	 * @return
	 * @throws IOException
	 */
	public static CharArraySet loadStopwordSet(File stopwords)
			throws IOException {
		Reader reader = null;
		try {
			reader = IOUtils.getDecodingReader(stopwords,
					StandardCharsets.UTF_8);
			CharArraySet res = new CharArraySet(Version.LUCENE_47, 0, false);
			WordlistLoader.getWordSet(reader,res);
			return res;
		} finally {
			IOUtils.close(reader);
		}
	}
	/**
	 * Kiểm tra một chuỗi chỉ là các chữ cái
	 * @param str
	 * @return
	 */
	public static boolean checkIsAlpha(String str) {
		int chr = 97;
		for (int i = chr; i < chr + 26; i++) 
			if (str.trim().equals(String.valueOf(Character.toChars(i)[0]))) {
				return true;
			}
		
		return false;
	}
	
	/**
	 * Kiểm tra 1 chuỗi có phải chuỗi số không
	 * @param str
	 * @return
	 */
	public static boolean checkIsDigit(String str) {
		try {
			Integer.parseInt(str.trim());
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
	
	/**
	 * 	Kiểm tra một chuỗi có chứa một chuỗi con hay không (cho phep khoảng cách các từ trong chuỗi con là n từ)
	 * @param text: văn bản tìm kiếm
	 * @param subPhrase: cụm từ tìm kiếm
	 * @param n: khoảng cách cho phép giữa các từ trong cụm
	 * @return: đoạn văn bản trong text chứa subPhrase
	 */
	public static String subStrNGram(String text, String subPhrase, int n) {
		boolean flag = false;
		int startIdx = 0, endIdx = 0;
		text = text.toLowerCase();
		subPhrase = subPhrase.toLowerCase();

		int idx = text.indexOf(subPhrase);
		if (idx >= 0) {
			startIdx = idx;
			endIdx = idx + subPhrase.length();
			flag = true;
		} else {
			String[] words = subPhrase.split(" ");
			String endWord = words[words.length - 1];
			while (text.length() > 0 && text.indexOf(endWord) >= 0) {
				int i;
				for (i = words.length - 1; i > 0 ; i--) {
					int idx1 = text.indexOf(words[i]);
					int idx2 = text.substring(0, idx1).lastIndexOf(words[i - 1]);
					if (idx2 < 0)
						break;
					
					if (i == words.length - 1)
						endIdx = idx1 + words[i].length();
					else if (i == 1)
						startIdx = idx2;
					int count = text.substring(idx2, idx1).split(" ").length;
					if (count > n + 1)
						break;
				}

				if (i == 0) {
					flag = true;
					break;
				} else {
					text = text.substring(text.indexOf(endWord) + endWord.length());
				}
			}
		}

		if (flag == true)
			return text.substring(startIdx, endIdx);
		
		return "";
	}
	
	/**
	 * Kiểm tra một phần tử có nằm trong mảng không
	 * @param aWord
	 * @param word
	 * @return
	 */
	public static boolean checkWordInArray(String[] aWord, String word) {
		boolean flag = false;
		for (String w: aWord) {			
			if (w.trim().equals(word.trim())) {
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	/**
	 * kiem tra mot chuoi co nam trong danh sach hay khong
	 * @param lstWords danh sach 
	 * @param word chuoi can tim
	 * @return
	 */
	public static boolean checkWordInList(List<String> lstWords, String word) {
		
		if (lstWords == null) {
			return true;
		}
		
		boolean flag = false;
		for (String w: lstWords) {		
			if (w.trim().equals(word.trim())) {
				flag = true;
				break;
			}
		}
		
		return flag;
	}
	
	/**
	 * Đếm tần số xuất hiện của một từ khóa trong một đoạn văn bản, nếu keyword = "" thì sẽ đếm số từ của văn bản. 
	 * @param text: văn bản thực hiện đếm số từ khóa 
	 * @param keyword: từ khóa cần đếm trong văn bản
	 * @return: Số lương từ keyword xuất hiện trong văn bản text (không phân biệt chữ hoa, chữ thường)
	 */
	public static int CountWord(String tokenizedText, String keyword){
		int count = 0;
		
		if (keyword.equals("")){
			return 0;
		} else {
			String[] words = tokenizedText.split(" ");
			for (String w: words) {
				w = " " + w + " ";
				keyword = " " + keyword + " ";
				if (w.equals(keyword))
					count++;
			}
		}
		
		return count;
	}
	
	/**
	 * tinh Cos cua 2 vector (2 cot) bat ki trong ma tran term Doc
	 * @param termDoc ma tran term doc chua trong so tf*idf
	 * @return
	 */
	public static double[] Cosine(double[][] termDoc) {
		int n = termDoc[0].length;
		int m = termDoc.length;
		
		double[] result = new double[n];
		
		double[] query = new double[m];
		for(int j = 0; j < m; j++)
		{
			query[j] = termDoc[j][n-1];
		}
		double qSqrt = 0.0;
		for (int i = 0; i < m; i++)
			qSqrt += query[i] * query[i];
		qSqrt = Math.sqrt(qSqrt);
		
		for (int i = 0; i < n - 1; i++) {
			double sum = 0.0;
			double docSqrt = 0.0;
			for (int j = 0; j < m; j++) {
				double tmp = termDoc[j][i];
				sum += query[j] * tmp;
				docSqrt += tmp * tmp;
			}
			
			docSqrt = Math.sqrt(docSqrt);
			if(qSqrt == 0 || docSqrt == 0)
				result[i] = 0;
			else
				result[i] = sum / (qSqrt * docSqrt);
		}
		
		return result;
	}	
}