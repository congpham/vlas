//Ref: http://www.codeproject.com/Articles/405128/Matrix-operations-in-Java
package util;

public class MatrixCalculator {
	//Chuyển vị ma trận
	public static double[][] transpose(double[][] a, int row, int col) {
		double[][] result = new double[col][row];
		
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++)
				result[j][i] = a[i][j];
		}
		
		return result;
	}
	
	//Tính định thức ma trận
	public static double determinant(double[][] a, int n) {
		double result = 0.0;
		if (n == 1) {
			result = a[0][0];
		}
		else if (n == 2) {
			result = a[0][0] * a[1][1] - (a[0][1] * a[1][0]);
		} else {
			for (int i = 0; i < n; i++)
				result += changeSign(i) * a[0][i] * determinant(createSubMatrix(a, n, n, 0, i), n - 1);
		}
		
		return result;
	}
	
	private static int changeSign(int i) {
		return (i % 2 == 0 ? 1 : -1);
	}
	
	private static double[][] createSubMatrix(double a[][], int row, int col, int excluding_row, int excluding_col) {
		double[][] result = new double[row - 1][col - 1];
		
		int r = -1;
		for (int i = 0; i < row; i++) {
			if (i == excluding_row)
				continue;
			r++;
			int c = -1;
			for (int j = 0; j < col; j++) {
				if (j == excluding_col)
					continue;
				result[r][++c] = a[i][j];
			}
		}
		
		return result;
	}
	
	public static double[][] cofactor(double[][] a, int n) {
		double[][] result = new double[n][n];
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++)
				result[i][j] = changeSign(i) * changeSign(j) * determinant(createSubMatrix(a, n, n, i, j), n - 1);
		}
		
		return result;
	}
	
	public static double[][] multiplyByConstant(double[][] a, int row, int col, double constant) {
		double[][] result = new double[row][col];
		
		for (int i = 0; i < row; i++)
			for (int j = 0; j < col; j++)
				result[i][j] = constant * a[i][j];
		
		return result;
	}
	
	//Tính nghịch đảo ma trận vuông
	public static double[][] inverse(double[][] a, int n) {
		return multiplyByConstant(transpose(cofactor(a, n), n, n), n, n, 1.0/determinant(a, n));
	}
	
	//Tính tích hai ma trận
	public static double[][] multiply(double[][] a, double[][] b, int rowa, int colrowab, int colb) {
		double[][] result = new double[rowa][colb];
		for (int i = 0; i < rowa; i++) {
			for (int j = 0; j < colb; j++) {
				double sum = 0.0;
				for (int k = 0; k < colrowab; k++)
					sum += a[i][k] * b[k][j];
				
				result[i][j] = sum;
			}
		}
		return result;
	}
}
