package entities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FloatField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.flexible.standard.StandardQueryParser;
import org.apache.lucene.queryparser.flexible.standard.config.StandardQueryConfigHandler;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;












import constant.IConstant;
//import ggsearch.*;
import util.Utils;

public class Search {
	private File indexDir;
	private String queryString;
	private String field;

	/**
	 * Hàm tạo, nếu muốn tìm kiếm các tập tin local
	 * @param indexDir: thư mục chưa các tập tin đã index
	 * @param queryString: chuỗi từ khoá truy vấn
	 * @param field: tên field được đặt khi index
	 */
	public Search(String indexDir, String queryString) {
		this.indexDir = new File(indexDir);
		this.queryString = queryString.trim();
	}
	
	

	public ScoreDoc[] searchDocs(IndexSearcher searcher, String field) throws Exception
	{

		//Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_47);
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_47,Utils.loadStopwordSet(new File(IConstant.STOPWORD)));
		//BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(this.queryString), "UTF-8"));
		//QueryParser parser = new QueryParser(Version.LUCENE_47, field, analyzer);
		
		//Query query = parser.parse(this.queryString);
		
		//StandardQueryParser qpHelper = new StandardQueryParser();
		//StandardQueryConfigHandler config = (StandardQueryConfigHandler)qpHelper.getQueryConfigHandler();
		//config.set(StandardQueryConfigHandler.ConfigurationKeys.ANALYZER,analyzer);
		BooleanQuery finalQuery = new BooleanQuery();	
		
		QueryParser qContent = new QueryParser(Version.LUCENE_47,"article", analyzer);
		Query contQuery = qContent.parse(this.queryString);// contQP.parse(this.queryString);
		contQuery.setBoost((float) 2);
		finalQuery.add(contQuery, Occur.MUST);
		
		QueryParser qName = new QueryParser(Version.LUCENE_47,"name", analyzer);
		Query nameQuery = qName.parse(this.queryString);
		nameQuery.setBoost((float)1.5);
		finalQuery.add(nameQuery, Occur.SHOULD);
		
		QueryParser qChap = new QueryParser(Version.LUCENE_47,"chapter", analyzer);
		Query chapQuery = qChap.parse(this.queryString);
		chapQuery.setBoost((float)1);
		finalQuery.add(chapQuery, Occur.SHOULD);
		
		QueryParser qSec = new QueryParser(Version.LUCENE_47,"section", analyzer);
		Query secQuery = qSec.parse(this.queryString);
		secQuery.setBoost((float)1);
		finalQuery.add(secQuery, Occur.SHOULD);
		
		TopDocs topDocs = searcher.search(finalQuery, null, 10);
		
		return topDocs.scoreDocs;
	}
	
	public boolean checkExist(List<ScoreDoc> finalDocs, ScoreDoc doc)
	{
		boolean flag = false;
		for(ScoreDoc d: finalDocs)
		{
			if(d.doc == doc.doc)
				return true;
		}
		return flag;
	}
	
	public void selectDoc(IndexSearcher searcher, String field, List<ScoreDoc> finalDocs) throws Exception
	{
		ScoreDoc[] scoreDocs = searchDocs(searcher,field);
		
		for(int i = 0; i<scoreDocs.length;i++)
		{
			
			if(!checkExist(finalDocs,scoreDocs[i]))
			{		
				finalDocs.add(scoreDocs[i]);
			}
		}
	}
	
	public List<Document> search() throws Exception {		
		IndexReader reader = DirectoryReader.open(FSDirectory.open(this.indexDir));
		IndexSearcher searcher = new IndexSearcher(reader);
		
		//ket qua tra ve la danh sach cac Document
		List<Document> lstDoc = new ArrayList<Document>();
		//danh sach tai lieu ScoreDoc thoa first threshold
		List<ScoreDoc> finalDocs = new ArrayList<ScoreDoc>();
		//danh sach tai lieu ScoreDoc thoa second threshold
		//List<ScoreDoc> secondaryDocs = new ArrayList<ScoreDoc>();
		//sap xep lai tu FinalDocs
		List<ScoreDoc> sortFinal = new ArrayList<ScoreDoc>();
		//selectDoc(searcher, "name", finalDocs);
		//selectDoc(searcher, "chapter", finalDocs);
		//selectDoc(searcher, "section", finalDocs);
		//selectDoc(searcher, "article", finalDocs);
		/*
		int size = finalDocs.size();
		if(size > IConstant.answerExtractionDocNumber)
			size = IConstant.answerExtractionDocNumber;
		for(int i = 0; i < size; i++)
		{
			float max = finalDocs.get(0).score;
			int pos = 0;
			for(int j = 1; j < finalDocs.size(); j++)
			{
				if(max < finalDocs.get(j).score)
				{
					max = finalDocs.get(j).score;
					pos = j;
				}
			}
			sortFinal.add(finalDocs.get(pos));
			finalDocs.remove(pos);
		}*/
		ScoreDoc[] scoreDocs = searchDocs(searcher,field);
		for(int i=0;i<scoreDocs.length;i++)
		{
			Document doc = searcher.doc(scoreDocs[i].doc);
			doc.add(new FloatField("score", scoreDocs[i].score, Field.Store.YES));
			lstDoc.add(doc);
		}
		return lstDoc;
	}
}

