package entities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Point {
	private List<String> points;//cac diem dc phan vao trong mang
	private String content;//toan bo noi dung cac diem;
	public static String regexp="\\w+\\s*[)]\\s*[\\w+ .,;]+\\s*[;.]\\s*";
	public String nameClause;
	private double score;
	private String title;
	private String ArticleName;
	
	public Point(){
		this.score = 0;
		this.content = null;
		this.title = null;
		this.ArticleName = null;
	}
	
	public void setTitle(String t)
	{
		this.title = t;
	}
	
	public String getTitle()
	{
		return this.title;
	}
	
	public void setArticleName(String name)
	{
		this.ArticleName = name;
	}
	
	public String getArticleName()
	{
		return this.ArticleName;
	}
	public void setScore(double score)
	{
		this.score = score;
	}
	
	public double getScore()
	{
		return this.score;
	}
	public Point(String text, String nameClause){
		this.nameClause = nameClause;
		this.content=nameClause + "\n" + text;
	}
	public List<String> getPoints() {
		return points;
	}
	public void setPoints(List<String> points) {
		this.points = points;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
}