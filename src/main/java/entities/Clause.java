package entities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Clause implements Comparable{
	String name; // nhung tu bo nghia cho trang thai dong tu : duoc, phai, chi
					// duoc, khong duoc, cam, tru truong hop... (pho tu)
	String contents;// noi dung cua khoan
	List<Point> points; //Luu tru cac diem trong khoan, khong phan biet loai nao.
	double score;
	private String title;
	private String ArticleName;
	public static String regexp="\\d+\\s*[.]";//\\s*[\\w+ .,;]+\\s*.\\s*";
	
	
	public Clause(){
		this.name=null;
		this.contents=null;
		this.score = 0;
		this.title = null;
		this.ArticleName = null;
	}
	
	public Clause(String text){
		
		Matcher match = Pattern.compile(Clause.regexp,Pattern.UNICODE_CHARACTER_CLASS).matcher(text);
		int start_name = 0;
		if(match.find())
		{
			start_name = match.start();
		}
		this.name = "khoản " + text.substring(start_name, start_name + 1);
		this.contents=text;
		Matcher m=Pattern.compile(Point.regexp, Pattern.UNICODE_CHARACTER_CLASS).matcher(text);
		int c=0;
		int start=0;
		this.points=new ArrayList<Point>();
		while (m.find()){
			c++;
			if (c==1){
				start=m.start();
				continue;
			}
			
			String p=text.substring(start, m.start());
			points.add(new Point(p,this.name));
			start=m.start();
		}
		
		if (c>0){
			String p=text.substring(start, text.length()-1);
			points.add(new Point(p,this.name));
		}		
	}
	
	public void setTitle(String t)
	{
		this.title = t;
	}
	
	public String getTitle()
	{
		return this.title;
	}
	
	public void setArticleName(String name)
	{
		this.ArticleName = name;
	}
	
	public String getArticleName()
	{
		return this.ArticleName;
	}
	public void setScore(double score)
	{
		this.score = score;
	}
	
	public double getScore()
	{
		return this.score;
	}
	
	public String getContent() {
		return contents;
	}

	public void setContent(String contents) {
		this.contents = contents;
	}
		

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public void setPoints(List<Point> lstPoint)
	{
		this.points = lstPoint;
	}
	
	public List<Point> getPoints()
	{
		return this.points;
	}

}
