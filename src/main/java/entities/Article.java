package entities;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Article implements Comparable{
	String name;// ten dieu luat

	String art_num;// so thu tu cua dieu
	String sec_num;// thuoc muc nao
	String chap_num;// thuoc chuong nao
	String doc_title;// thuoc van ban nao
	double boost;
	
	List<Clause> clauses; // danh sach cac khoan trong dieu luat

	String content;// noi dung toan bo dieu luat
	String score;
	
	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public static String regexp = "\\nđiều\\s+\\d+\\s*\\.\\s+[\\w+ ,;]+\\s+";//Chuỗi nhận diện từng điều
	public static String regChapter = "\nchương\\s+[0-9A-Z]+\\s+[\\w+ ,;]+\\s*";  // chuỗi nhận diện từng chương					
	public static String regSection  = "\nmục\\s+\\d+\\s*[:.\\s][\\w+ ,;]+\\s*"; //chuỗi nhận diện từng mục
	public boolean IsNull()
	{
		if(this.name.equals("") 
				&& this.content.equals("") 
				&& this.art_num.equals("")
				&& this.doc_title.equals("") 
				&& this.sec_num.equals("")
				&& this.chap_num.equals(""))
			return true;
		return false;
	}
	/**
	 * 
	 */
	public Article(String text) {
		//Pattern patChap = Pattern.compile(regChapter, Pattern.UNICODE_CHARACTER_CLASS);
		//Matcher matchChap = patChap.matcher(text);
		this.chap_num = "";
		this.sec_num = "";
		String regex = "điều\\s+\\d+\\s*[.]";
		Pattern pat = Pattern.compile(regex, Pattern.UNICODE_CHARACTER_CLASS);
		Matcher match = pat.matcher(text);
		int start = 0;
		if (match.find()) {
			start = match.start();
		}
		Pattern pattern = Pattern.compile(regexp, Pattern.UNICODE_CHARACTER_CLASS);
		Matcher m = pattern.matcher(text);
		int end = 0;
		if (m.find()) {
			end = m.end();
		}
		if (start > 0 && end > 0) {
			this.name = text.substring(start, end);
		}

		this.content = text;
		if(match.start() > 0 && match.end() > 0)
		{
			String pre = text.substring(match.start(), match.end());

			match = Pattern.compile("\\d+").matcher(pre);
			if (match.find()) {
				this.art_num = pre.substring(match.start(), match.end());
			}
		}			
		//Loc ra cac khoan
		String rule=text.substring(m.end(), text.length()-1);
		match = Pattern.compile(Clause.regexp,Pattern.UNICODE_CHARACTER_CLASS).matcher(rule);
		int start_clause = 0;
		int c = 0;
		clauses=new ArrayList<Clause>();
		while (match.find()) {
			c++;
			if (c == 1) {
				start_clause = match.start();
				continue;
			}
			String text_clause = rule.substring(start_clause, match.start());
			Clause cl=new Clause(text_clause);
			clauses.add(cl);
			start_clause = match.start();
		}
		//System.out.print("Start pos = "+start_clause);
		if (c > 0) {
			Clause cl=new Clause(rule.substring(start_clause,
					rule.length() - 1));
			clauses.add(cl);
		} else {
			Clause cl = new Clause(rule);
			clauses.add(cl);
		}

	}

	public Article() {
		clauses=new ArrayList<Clause>();
		this.chap_num = "";
		this.sec_num = "";
		this.art_num = "";
		this.name = "";
		this.content = "";
		this.boost = 0;
		this.doc_title = "";
	}
	
	public double getBoost()
	{
		return this.boost;
	}
	
	public void setBoost(double boost)
	{
		this.boost = boost;
	}
	
	public List<Clause> getClauses() {
		return clauses;
	}

	public void setClauses(List<Clause> clauses) {
		this.clauses = clauses;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArt_num() {
		return art_num;
	}

	public void setArt_num(String art_num) {
		this.art_num = art_num;
	}

	public String getSec_num() {
		return sec_num;
	}

	public void setSec_num(String sec_num) {
		this.sec_num = sec_num;
	}

	public String getChap_num() {
		return chap_num;
	}

	public void setChap_num(String chap_num) {
		this.chap_num = chap_num;
	}

	public String getDoc_title() {
		return doc_title;
	}

	public void setDoc_title(String doc_title) {
		this.doc_title = doc_title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

}