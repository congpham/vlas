package entities;

import java.util.ArrayList;
import java.util.List;

import util.MatrixCalculator;
import weka.core.matrix.Matrix;
import weka.core.matrix.SingularValueDecomposition;


public class LSImodel {
	private int numFactor; // so chieu khong gian LSI
	private int termsNum; // so dong cua term doc matrix
	private int docsNum; // so cot term doc matrix
	private double[][] termDocMatrix;
	private double[][] termMatrix; //U
	private double[][] docsMatrix; //V
	private double[][] eigenValueMatrix; //S
	
	/**
	 * Hàm tạo
	 * @param termDocMatrix: Document Matrix, vector cột cuối là vector của câu query
	 * @param row: số mục từ
	 * @param col: số tài liệu + 1 (câu truy vấn)
	 * @param numFactor: giá trị k trong LSI
	 */
	public LSImodel(double[][] termDocMatrix, int row, int col, int numFactor) {
		this.termDocMatrix = termDocMatrix;
		this.numFactor = numFactor;
		this.termsNum = row;
		this.docsNum = col - 1; //phần từ thứ col là vector của câu query
		/*
		double[][] tmp = new double[row][col - 1];
		for (int i = 0; i < row; i++)
			for (int j = 0; j < col - 1; j++)//cột cuối là của câu query
				tmp[i][j] = termDocMatrix[i][j];
		*/
		//Svd matrix
		Matrix matrix = new Matrix(this.termDocMatrix);
		SingularValueDecomposition svd = new SingularValueDecomposition(matrix);
		this.eigenValueMatrix = svd.getS().getArray();
		this.termMatrix = svd.getU().getArray();
		this.docsMatrix = svd.getV().getArray();
		
//		System.out.println("--------------------U-----------------");
//		svd.getU().print(4, 2);
//		System.out.println("--------------------S-----------------");
//		svd.getS().print(4, 2);
//		System.out.println("--------------------V-----------------");
//		svd.getV().print(4, 2);
	}
	
	
	/**
	 * lay cot cua term document matrix tai vi tri idx
	 * @param idx
	 * @return
	 */
	private double[][] getDocAt(int idx) {
		double[][] result = new double[1][this.termsNum];
		
		for (int i = 0; i < this.termsNum; i++)
			result[0][i] = this.termDocMatrix[i][idx];
		
		return result;
	}
	
	/**
	 * lay ma tran Uk da giam so chieu theo termFactor(chieu ko gian LSI)
	 * @return
	 */
	private double[][] getUk() {
		double[][] result = new double[this.termsNum][this.numFactor];
		
		for (int i = 0; i < this.termsNum; i++)
			for (int j = 0; j < this.numFactor; j++)
				result[i][j] = this.termMatrix[i][j];
		
		return result;
	}
	
	/**
	 * lay ma tran Sk da giam so chieu theo termFactor(chieu ko gian LSI)
	 * @return
	 */
	private double[][] getSk() {
		double[][] result = new double[this.numFactor][this.numFactor];
		
		for (int i = 0; i < this.numFactor; i++)
			for (int j = 0; j < this.numFactor; j++)
				result[i][j] = this.eigenValueMatrix[i][j];
		
		return result;
	}
	
	/**
	 * dua document & query ve chung 1 khong gian LSI
	 * @return
	 */
	public List<double[]> switchToLsiSpace() {
		List<double[]> result = new ArrayList<double[]>();
		
		for (int i = 0; i < this.docsNum + 1; i++) {
			double[] item = new double[this.numFactor];
			
			double[][] doc = this.getDocAt(i);
			double[][] uK= this.getUk();
			double[][] sK = this.getSk();
			Matrix skMatrix = new Matrix(sK);
			sK = skMatrix.inverse().getArray();
			
			double[][] tmp = MatrixCalculator.multiply(doc, uK, 1, this.termsNum, this.numFactor);
			tmp = MatrixCalculator.multiply(tmp, sK, 1, this.numFactor, this.numFactor);
			String vector = "(";
			for (int k = 0; k < this.numFactor; k++) {
				item[k] = tmp[0][k];
				if (vector != "(")
					vector += ", ";
				vector += item[k];
			}
			vector += ")";
//			System.out.println(vector);
			
			result.add(item);
		}
		
		return result;
	}
	
	/**
	 * tinh Sim cua 2 document bat ki trong khong gian LSI
	 * @param lsiSp
	 * 		mang khong gian LSI da duoc rut gon chieu
	 * @return 
	 * 		
	 */
	public double[] calculateSim(List<double[]> lsiSp) {
		double[] result = new double[this.docsNum];
		
		double[] query = lsiSp.get(lsiSp.size() - 1);
		double qSqrt = 0.0;
		for (int i = 0; i < this.numFactor; i++)
			qSqrt += query[i] * query[i];
		qSqrt = Math.sqrt(qSqrt);
		
		for (int i = 0; i < lsiSp.size() - 1; i++) {
			double sum = 0.0;
			double docSqrt = 0.0;
			for (int j = 0; j < this.numFactor; j++) {
				double tmp = lsiSp.get(i)[j];
				sum += query[j] * tmp;
				docSqrt += tmp * tmp;
			}
			
			docSqrt = Math.sqrt(docSqrt);
			
			result[i] = sum / (qSqrt * docSqrt);
		}
		
		return result;
	}	
}
