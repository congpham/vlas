package entities;


import java.io.File;

import java.io.IOException;




import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import constant.IConstant;
import util.Utils;

public class Index {
	private List<Article> lstArticle;
	/***
	 * Indexer Constructor
	 * @param dataDir: The directory contains data for indexing
	 * @param indexDir: The directory saves indexed data
	 */
	public Index(List<Article> lstArticle) {
		this.lstArticle = lstArticle;
		
	}
	
	
	
	/***
	 * Main indexing function
	 * @param isCreated: indexing for new or updating
	 * @return A number of files were indexed
	 */
	public int indexdata(boolean isCreated) {
		try {
			Directory dir = FSDirectory.open(new File(IConstant.INDEX_DIR));

			//Analyzer analyzer = new VietAnalyzer(IConstant.STOPWORD);
			//IndexWriterConfig iwc = new IndexWriterConfig(Version.LATEST, analyzer);
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_47,Utils.loadStopwordSet(new File(IConstant.STOPWORD)));
			IndexWriterConfig iwc = new IndexWriterConfig(
					Version.LUCENE_47, analyzer);
			if (isCreated) {
				iwc.setOpenMode(OpenMode.CREATE);
			} else {
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			}
			IndexWriter writer = new  IndexWriter(dir, iwc);
			indexFile(writer,this.lstArticle);
			int numIndexed = writer.numDocs();
			writer.close();

			return  numIndexed;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return 0;
	}

	/***
	 * Indexing File
	 * @param writer: IndexWriter object
	 * @param f is an indexing file
	 * @throws IOException: exception if file does not exist
	 */
	private void indexFile(IndexWriter writer, List<Article> lstAticle) throws IOException {		

			for(Article article : lstArticle)
			{
				if(!article.IsNull())
				{
					Document doc = new Document();
					System.out.println("Start Indexing Article: " + article.getName());
					double boost = article.getBoost();
					TextField title = new TextField("title",article.getDoc_title(),Field.Store.YES);
					title.setBoost((float)boost);
					doc.add(title);
					//doc.add(new TextField("chapter",article.getChap_num(),Field.Store.YES));
					TextField id = new TextField("id",article.getArt_num(),Field.Store.YES);
					id.setBoost((float)boost);
					doc.add(id);
					TextField name = new TextField("name",article.getName(),Field.Store.YES);
					name.setBoost((float)boost);
					doc.add(name);
					TextField articlecontent = new TextField("article",article.getContent(),Field.Store.YES);
					articlecontent.setBoost((float)boost);
					doc.add(articlecontent);
					
					if(!article.getChap_num().equals(""))
					{
						TextField chapter = new TextField("chapter",article.getChap_num(),Field.Store.YES);
						chapter.setBoost((float)boost);
						doc.add(chapter);
					}
					if(!article.getSec_num().equals(""))
					{
						TextField section = new TextField("section",article.getSec_num(),Field.Store.YES);
						section.setBoost((float)boost);
						doc.add(section);
					}
					/*
					List<Clause> lstClause = article.getClauses();
					if(lstClause.size() > 0)
					{
						System.out.println("Start Indexing Clause in Article: "+ article.getName());
						for(Clause clause : lstClause)
						{				
							doc.add(new TextField("clause",clause.getContents(),Field.Store.YES));
							
							
							List<Point> lstPoint = clause.getPoints();
							
							if(lstPoint.size() > 0)
								for(Point point : lstPoint)
								{
									if(!point.getContent().equals(""))
										doc.add(new TextField("point",point.getContent(),Field.Store.YES));
								}
						}
					}*/
					if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
						// New index, so we just add the document (no old
						// document can be there):
						writer.addDocument(doc);
					} else {
						// Existing index (an old copy of this document may have
						// been indexed) so
						// we use updateDocument instead to replace the old one
						// matching the exact
						// path, if present:
						writer.updateDocument(new Term("name", article.getName()), doc);
					}
				}
			}
			
	}

	/***
	 * Check file format, this function can be overrided for other supported formats
	 * @param f is a checking file
	 * @return
	 */
	protected boolean checkFileExtension(File f) {
		return f.getName().endsWith(".txt");
	}
	

}
