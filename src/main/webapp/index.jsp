<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>VLAS - Vietnamese Law Question Answering System</title>
<style type="text/css">
	#question {
	    width: 600PX;
	    height: 20px;
	    text-align: left;
	    vertical-align: middle;
	    font-size: 14px;
	    font-style: italic;
	}
	
	.controls input[type="submit"] {
		width: 110px;
		height: 24px;
		text-align: center;
		vertical-align: middle;
		margin: 10px;
		
		font-weight: bold;
		font-size: 14px;
	}
	
	.copyright {
		font-size: 12px;
	}
	
	table tr td a {
		text-decoration: inherit;
		color: #6F57EE;
	}
	
	table tr td i a {
		color: navy;
		font-weight: bold;
	}
	
	.logo {
		margin-top: 80px;
		margin-bottom: 30px;
	}
</style>
<script type="text/javascript">
	function getQuestion(me) {
		var question = document.getElementById("question");
		question.value = me.innerText.split(". ")[1];
	}
</script>
</head>
<body>
	<div id="question-demo"></div>
	<f:form action="ans" theme="simple" method="post">
		<table align="center" width="780px">
			<tr>
				<td colspan="2" align="center">
					<img alt="A Vietnam Law Question Answering System" src = "images\\Logo.PNG" class="logo">
				</td>
			</tr>		
			<tr>
				<td class="controls" align="center">
					<input type="text" name="question" id="question" placeholder="Vui lòng nhập một câu hỏi."></input>
					
										
				</td>
			</tr>
			<tr>
				<td class="controls" colspan="2" align="center">
					<input type="submit"  value="Hỏi" ></input>					
				</td>
			</tr>
			
			<tr>
				<td align="center" class="copyright">
					Nguyễn Trí Luận Ngữ - Phạm Thành Công &copy 2015, <a href="http://www.hcmus.edu.vn">University of Science</a>, VNU-HCMC
				</td>
			</tr>
			<tr>
			<td colspan="2">
					<br />
					<b>Ví dụ.</b>
			</td>
			</tr>			
			<tr>
				<td>
					<i>
					<a href="#question-demo" onclick="getQuestion(this);">1. Trường hợp nào doanh nghiệp bị thu hồi Giấy chứng nhận đăng ký kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">2. Đăng ký đổi tên doanh nghiệp được thực hiện như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">3. Doanh nghiệp có nghĩa vụ gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">4. Quy định phối hợp tạo và cấp mã số doanh nghiệp quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">5. Doanh nghiệp bị giải thể trong các trường hợp nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">6. Cho hỏi tập đoàn kinh tế là gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">7. Nhóm công ty là loại hình như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">8. Xin cho biết thủ tục cấp lại giấy đăng ký kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">9. Xin cho biết trình tự đăng ký kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">10. Xin cho biết những ngành, nghề nào bị cấm kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">11. Công ty TNHH 2 thành viên có được giảm vốn điều lệ?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">12. Xin cho biết thủ tục thay đổi người đại diện theo pháp luật của công ty TNHH 1 thành viên?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">13. Giấy đề nghị đăng ký kinh doanh bao gồm những nội dung gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">14. Việc đặt tên doanh nghiệp được pháp luật quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">15. Việc xử lý tranh chấp về tên doanh nghiệp được thực hiện như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">16. Thành viên của công ty trách nhiệm hữu hạn 2 thành viên trở lên có quyền gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">17. Những việc doanh nghiệp cần làm sau khi được cấp giấy chứng nhận đăng ký doanh nghiệp?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">18. Việc đăng ký thay đổi người đại diện theo pháp luật của công ty trách nhiệm hữu hạn, công ty cô phần được pháp luật quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">19. Theo quy định của luật doanh nghiệp, chủ doanh nghiệp tư nhân có quyền quyết định việc quản lý doanh nghiệp không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">20. Pháp luật qui định sát nhập công ty như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">21. Các ngành nghề kinh doanh nào phải có vốn pháp định trước khi đăng ký kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">22. Xin cho biết quy định về các giấy tờ chứng thực cá nhân trong hồ sơ đăng ký kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">23. Thế nào là các ngành nghề kinh doanh có điều kiện, có chứng chỉ hành nghề?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">24. Hồ sơ đăng ký hoạt động chi nhánh, văn phòng đại diện, thông báo lập địa điểm kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">25. Những ngành, nghề kinh doanh nào đòi hỏi phải có chứng chỉ hành nghề?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">26. Theo quy định hiện hành, hồ sơ đăng ký thành lập doanh nghiệp công ty TNHH một thành viên được quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">27. Hiện nay, hồ sơ đăng ký doanh nghiệp đối với công ty cổ phần được quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">28. Nhiệm vụ, quyền hạn, trách nhiệm của Phòng đăng ký kinh doanh cấp tỉnh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">29. Hồ sơ đăng ký doanh nghiệp đối với công ty TNHH có hai thành viên trở lên?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">30. Pháp luật quy định như thế nào về thời hạn doanh nghiệp thực hiện đăng ký thay đổi nội dung đăng ký kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">31. Định nghĩa doanh nghiệp nhỏ và vừa?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">32. Việc bổ sung thông tin trong hồ sơ đăng ký doanh nghiệp được quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">33. Việc chấm dứt hoạt động chi nhánh, văn phòng đại diện, địa điểm kinh doanh được quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">34. Việc hiệu đính thông tin trong giấy chứng nhận đăng ký doanh nghiệp được quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">35. Đăng ký bổ sung, thay đổi ngành, nghề kinh doanh được thực hiện như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">36. Việc cung cấp thông tin về đăng ký doanh nghiệp được quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">37. Công ty TNHH một thành viên có quyền tăng hay giảm vốn điều lệ hay không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">38. Việc giải quyết tranh chấp nội bộ được quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">39. Tổ chức, cá nhân nào không được quyền thành lập và quản lý doanh nghiệp tại Việt Nam?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">40. Công ty trách nhiệm hữu hạn 2 thành viên trở lên có được giảm vốn điều lệ hay không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">41. Tên doanh nghiệp như thế nào được xem là không hợp lệ?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">42. Thủ tục đăng ký đổi trụ sở chính của doanh nghiệp?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">43. Trường hợp doanh nghiệp làm mất giấy chứng nhận đăng ký kinh doanh thì có được cấp lại không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">44. Trường hợp nào thì tên của doanh nghiệp được xem là gây nhầm lẫn với tên một doanh nghiệp đã đăng ký?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">45. Trong thời gian bao lâu, doanh nghiệp phải công bố nội dung đăng ký kinh doanh kể từ thời điểm được cấp giấy chứng nhận đăng ký kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">46. Việc chuyển quyền sở hữu đối với tài sản góp vốn có phải chịu lệ phí trước bạ hay không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">47. Kể từ thời điểm bị thu hồi giấy chứng nhận đăng ký kinh doanh, trong thời gian bao lâu doanh nghiệp bắt buộc phải giải thể?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">48. Khi nào thì doanh nghiệp mới được cấp giấy chứng nhận đăng ký kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">49. Hồ sơ đăng ký kinh doanh của công ty hợp danh bao gồm những gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">50. Hồ sơ đăng ký kinh doanh của doanh nghiệp tư nhân bao gồm những gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">51. Hồ sơ đăng ký kinh doanh của công ty trách nhiệm hữu hạn bao gồm những gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">52. Theo quy định hiện nay, mức thu phí, lệ phí đăng ký thành lập doanh nghiệp tư nhân, công ty cổ phần, công ty TNHH MTV, công ty TNHH hai thành viên,... được quy định như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">53. Các quy định nào của pháp luật cấm cán bộ, công chức, viên chức thành lập và quản lý doanh nghiệp?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">54. Doanh nghiệp tư nhân có quyền chuyển đổi thành công ty trách nhiệm hữu hạn hay không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">55. Tổ chức, cá nhân nào không được quyền góp vốn vào công ty trách nhiệm hữu hạn?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">56. Trong trường hợp nào, giá chào bán cổ phần lớn hơn giá thị trường tại thời điểm chào bán hiện tại?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">57. Người được tặng cho phần vốn có đương nhiên trở thành thành viên công ty trách nhiệm hữu hạn hai thành viên trở lên không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">58. Xin cho biết mức lệ phí khi đăng ký doanh nghiệp?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">59. Công ty trách nhiệm hữu hạn trong quá trình hoạt động có được chuyển đổi thành công ty cổ phần hoặc ngược lại hay không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">60. Thủ tục chuyển đổi loại hình công ty?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">61. Thay đổi chủ tịch hội đồng thành viên như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">62. Cơ quan nào giải quyết tranh chấp giữa các nhà đầu tư?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">63. Thời hiệu khởi kiện tranh chấp thương mại là bao lâu?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">64. Ai có thẩm quyền bãi miễn Giám đốc trong công ty cổ phần?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">65. Thủ tục triệu tập Đại hội đồng cổ đông được thực hiện như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">66. Tài sản của công ty hợp danh gồm những gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">67. Công ty hợp danh tiếp nhận thành viên mới cần có những thủ tục gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">68. Những loại hình doanh nghiệp nào có thể chuyển đổi lẫn nhau?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">69. Ai là người đại diện theo pháp luật của doanh nghiệp tư nhân?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">70. Theo luật doanh nghiệp, tư cách thành viên chấm dứt khi nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">71. Trường hợp nào được xem là giải thể công ty?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">72. Công ty nào bắt buộc giám đốc phải là thành viên công ty?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">73. Công ty trách nhiệm hữu hạn một thành viên có quyền phát hành cổ phần không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">74. Số lượng thành viên của công ty TNHH 2 thành viên trở lên là bao nhiêu người?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">75. Trong công ty TNHH 2 thành viên trở lên, khi nào thì quyết định của HĐQT tại cuộc họp được thông qua?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">76. Đối với công ty TNHH 1 thành viên, chủ tịch hội đồng thành viên do ai chỉ định?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">77. Cơ quan nào có thẩm quyền quy định về ngành, nghề kinh doanh có điều kiện và điều kiện kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">78. Trường hợp các cổ đông sáng lập không thanh toán đủ số cổ phần đã đăng ký mua thì xử lý như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">79. Điều kiện kinh doanh được thể hiện dưới các hình thức nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">80. Nhà đầu tư nước ngoài có được phép kinh doanh dịch vụ khách sạn, nhà hàng tại Việt Nam hay không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">81. Luật doanh nghiệp quy định về cổ phần phổ thông của cổ đông như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">82. Cổ đông phổ thông có những quyền gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">83. Theo luật doanh nghiệp, cổ đông phổ thông có thể tham dự đại hội đồng cổ đông theo những hình thức nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">84. Việc chuyển nhượng phần vốn góp trong công ty trách nhiệm hữu hạn hai thành viên trở lên được thực hiện như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">85. Công ty TNHH một thành viên có quyền chuyển nhượng phần góp vốn của mình cho người khác được không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">86. Ngành, nghề nào cần có chứng chỉ hành nghề trước khi đăng ký kinh doanh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">87. Ai là người ký thông báo thay đổi người đại diện của công ty trách nhiệm hữu hạn một thành viên?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">88. Đăng ký bổ sung, thay đổi ngành, nghề kinh doanh được thực hiện như thế nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">89. Người nước ngoài muốn thành lập công ty tại Việt Nam thì phải đáp ứng những yêu cầu và thủ tục nào?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">90. Tổ chức xã hội - nghề nghiệp có thuộc đối tượng cấm góp vốn kinh doanh không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">91. Doanh nghiệp tư nhân, công ty hợp danh có được hợp nhất và sáp nhập không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">92. Quy định pháp luật về nghĩa vụ tài sản của công ty hợp danh và các thành viên công ty hợp danh?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">93. Điều kiện thành lập khu kinh tế cửa khẩu?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">94. Chủ doanh nghiệp tư nhân có quyền và nghĩa vụ gì khi bán doanh nghiệp tư nhân?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">95. Ai có quyền triệu tập hội đồng thành viên?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">96. Điều kiện kinh doanh dịch vụ logistic?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">97. Văn phòng đại diện có được khắc con dấu riêng không?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">98. Nội dung của bản danh sách thành viên, danh sách cổ đông sáng lập?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">99. Điều kiện làm thành viên Hội đồng quản trị là gì?</a><br />
					<a href="#question-demo" onclick="getQuestion(this);">100. Một hộ kinh doanh có thể có tối đa bao nhiêu lao động?</a><br />
										
					
					</i>
				</td>
			</tr>
		</table>
	</f:form>
</body>
</html>