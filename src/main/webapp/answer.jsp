<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="f" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>VLAS - A Vietnamese Law Question Answering System</title>
<style type="text/css">
	#question {
	    width: 600PX;
	    height: 20px;
	    text-align: left;
	    vertical-align: middle;
	    font-size: 14px;
	    font-style: italic;
	}
	
	.controls input[type="submit"] {
		width: 110px;
		height: 24px;
		text-align: center;
		vertical-align: middle;
		margin: 10px;
		
		font-weight: bold;
		font-size: 14px;
	}
	
	.copyright {
		font-size: 12px;
	}
	
	table tr td a {
		text-decoration: inherit;
		color: #6F57EE;
	}
	
	table tr td i a {
		color: navy;
		font-weight: bold;
	}
	
	.logo {
		margin-top: 80px;
		margin-bottom: 30px;
	}
</style>
<script type="text/javascript">
	function getQuestion(me) {
		var question = document.getElementById("question");
		question.value = me.innerText.split(". ")[1];
	}
</script>
</head>
<body>
	<div id="question-demo"></div>
	<f:form action="ans" theme="simple" method="post">
		<table align="center" width="780px">
			<tr>
				<td colspan="2" align="center">
				<a href="index.jsp">
					<img alt="A Vietnam Law Question Answering System" src = "images\\Logo.PNG" class="logo">
				</a>
				</td>
			</tr>		
			<tr>
				<td class="controls" align="center">
					<input type="text" name="question" id="question" placeholder="Vui lòng nhập một câu hỏi."></input>
					
										
				</td>
			</tr>
			<tr>
				<td class="controls" colspan="2" align="center">
					<input type="submit"  value="Hỏi" ></input>					
				</td>
			</tr>
			
			<tr>
				<td align="center" class="copyright">
					Nguyễn Trí Luận Ngữ - Phạm Thành Công &copy 2015, <a href="http://www.hcmus.edu.vn">University of Science</a>, VNU-HCMC
				</td>
			</tr>			
	
		</table>
	</f:form>
	<h1 style="margin-left: 280px"><b =>Câu hỏi</b>: </h1>
	<b style="margin-left: 280px">${ question }</b>
	
	<h1 style="margin-left: 280px"><b>Trả Lời</b>:</h1>
	<table align="center" width="780px">
	<tr>
		<td>
			${ result }
		</td>
	</tr>
	
	</table>
	
	
</body>
</html>